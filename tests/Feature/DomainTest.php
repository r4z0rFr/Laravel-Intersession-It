<?php

namespace Tests\Feature;

use App\Company;
use App\Domain;
use App\Project;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class DomainTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        Passport::actingAs(factory(User::class)->create());
    }
    /** @test */
    public function display_all_domains()
    {
        $response = $this->get('api/domain');

        $response->assertOk()
                ->assertSee(Domain::all());
    }

    /** @test */
    public function display_a_domain()
    {
        $response = $this->get('api/domain/1');

        $response->assertOk()
                ->assertSee(Domain::first()->name);
    }

    /** @test */
    public function create_a_domain()
    {
        $domain = factory('App\Domain')->make();

        $response = $this->post('api/domain', $domain->toArray());

        $response->assertOk()
                ->assertSee($domain['name']);
    }

    /** @test */
    public function update_a_domain()
    {
        $domain = factory('App\Domain')->create();

        $response = $this->put('api/domain/' . $domain->id, ['technical_level' => 2]);

        $response->assertOk()
                ->assertSee(2);
    }

    /** @test */
    public function delete_a_domain()
    {
        $domain = factory('App\Domain')->create();

        $response = $this->delete('api/domain/' . $domain->id);

        $response->assertStatus(204);
    }

    /** @test */
    public function add_domain_to_project()
    {
        $project = factory(Project::class)->create();

        $domain = $project->domains()->create(['name' => 'Developpement']);

        $this->assertTrue($domain->is($project->domains->first()));
    }

    /** @test */
    public function add_domain_to_user()
    {
        $user = factory(User::class)->create();

        $domain = $user->domains()->create(['name' => 'Developpement']);

        $this->assertTrue($domain->is($user->domains->first()));
    }

    /** @test */
    public function add_domain_to_company()
    {
        $company = factory(Company::class)->create();

        $domain = $company->domains()->create(['name' => 'Developpement']);

        $this->assertTrue($domain->is($company->domains->first()));
    }
}
