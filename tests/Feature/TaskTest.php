<?php

namespace Tests\Feature;

use App\Date;
use App\Project;
use App\Risk;
use App\Task;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class TaskTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        Passport::actingAs(factory(User::class)->create());
    }
    /** @test */
    public function display_all_tasks()
    {
        $response = $this->get('api/task');
        
        $response->assertOk()
                ->assertSee(Task::first()->name);
    }

    /** @test */
    public function display_a_tasks()
    {
        $response = $this->get('api/task/1');

        $response->assertOk()
                ->assertSee(Task::first()->name);
    }

    /** @test */
    public function create_a_tasks()
    {
        $task = factory('App\Task')->make()->toArray();

        $date = factory('App\Date')->make()->toArray();

        $collection = collect();

        $request = $collection->merge([$task, $date])->flattenKeepKeys();

        $response = $this->post('api/task', $request->toArray());

        $response->assertOk()
                ->assertSee($task['name']);
    }

    /** @test */
    public function update_a_tasks()
    {
        $task = factory('App\Task')->create();

        $response = $this->put('api/task/' . $task->id, ['name' => 'NewName']);

        $response->assertOk()
                ->assertSee('NewName');
    }

    /** @test */
    public function delete_a_tasks()
    {
        $task = factory('App\Task')->create();

        $response = $this->delete('api/task/' . $task->id);

        $response->assertStatus(204);
    }

    /** @test */
    public function create_undertasks()
    {
        $task = factory('App\Task')->create();

        $undertask = factory('App\Task')->make();

        $task->undertasks()->create($undertask->toArray());

        $this->assertContains($task->undertasks()->first()->name, $undertask->name);
    }

    /** @test */
    public function add_a_risk_to_a_task()
    {
        $task = factory(Task::class)->create();

        $risk = factory(Risk::class)->make();

        $task->risks()->create($risk->toArray());

        $this->assertContains($task->risks()->first()->name, $risk->name);
    }

    /** @test */
    public function add_date_to_task()
    {
        $task = factory(Task::class)->create();

        $date = factory(Date::class)->make();

        $task->date()->create($date->toArray());

        $this->assertEquals($task->date()->first()->date_begin, $date->date_begin);
    }

    /** @test */
    public function create_assignments()
    {
        $user = factory('App\User')->create();
        $task = factory('App\Task')->create();

        $task->assign($user);

        $this->assertTrue($task->users()->first()->is($user));
    }

    /** @test */
    public function get_task_price()
    {
        $project = Project::first();

        $task = $project->tasks()->create(factory('App\Task')->make()->toArray());

        $this->assertEquals(0, $task->getPrice());

        $task->date()->create(factory('App\Date')->make()->toArray());

        $this->assertNotEquals(0, $task->getPrice());
    }
}
