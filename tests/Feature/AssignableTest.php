<?php

namespace Tests\Feature;

use App\Project;
use App\Task;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AssignableTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function assign_user_to_a_project()
    {
        $project = factory(Project::class)->create();

        $project->assign($this->user);

        $this->assertTrue($project->isAssign($this->user));

        $this->assertTrue($project->users()->first()->is($this->user));
    }

    /** @test */
    public function assign_user_to_a_task()
    {
        $task = factory(Task::class)->create();

        $task->assign($this->user);

        $this->assertTrue($task->isAssign($this->user));
    }

    /** @test */
    public function get_user_from_project()
    {
        $project = factory(Project::class)->create();

        $project->assign($this->user);

        $user = $project->users()->get()->first();

        $this->assertTrue($user->is($this->user));
    }
}
