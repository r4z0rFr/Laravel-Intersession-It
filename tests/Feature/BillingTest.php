<?php

namespace Tests\Feature;

use App\Billing;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class BillingTest extends TestCase
{
  public function setUp()
  {
    parent::setUp();

    Passport::actingAs(factory(User::class)->create());
  }

  /** @test */
  public function display_all_billing()
  {
    $response = $this->get('api/billing');

    $response->assertOk()->assertSee(Billing::all());
  }

  /** @test */
  public function display_a_billing()
  {
      $response = $this->get('api/billing/1');
      $response->assertOk()->assertSee(Billing::first()->project_name);
  }

  /** @test */
  public function create_a_billing()
  {
      $billing = factory('App\Billing')->make();

      $response = $this->post('api/billing', $billing->toArray());

      $response->assertOk()->assertSee($billing['project_name']);



  }

  /** @test */
  public function update_a_billing()
  {
      $billing = factory('App\Billing')->create();

      $response = $this->put('api/billing/' . $billing->id,
                              [
                                'project_name'    => "Meowing out loud!",

                                'company_name'    => "Le Kitty Company!",
                                'company_address' => "12 street of the meowing kitten 91400 purringCity",
                                'company_phone'   => "06 14 74 86 09",
                                'company_email'   => "catforever@gmail.com",
                                'company_siret'   => "732 829 320 00074",

                                'customer_name'   => "Mister Cat",
                                'customer_email'  => "kittycat@gmail.com",
                                'customer_phone'  => "01 69 85 56 71",
                                'customer_siret'  => "732 829 320 00074",

                                'total_price'     => 285.65,
                                'total_budget'    => 45.65,
                                'billing_date'    => "2019-06-01 19:23:52"

                              ]
                          );
      $response->assertOk()->assertSee("Meowing out loud!");

      $response->assertOk()->assertSee("Le Kitty Company!");
      $response->assertOk()->assertSee("12 street of the meowing kitten 91400 purringCity");
      $response->assertOk()->assertSee("06 14 74 86 09");
      $response->assertOk()->assertSee("catforever@gmail.com");
      $response->assertOk()->assertSee("732 829 320 00074");

      $response->assertOk()->assertSee("Mister Cat");
      $response->assertOk()->assertSee("kittycat@gmail.com");
      $response->assertOk()->assertSee("01 69 85 56 71");
      $response->assertOk()->assertSee("732 829 320 00074");

      $response->assertOk()->assertSee(285.65);
      $response->assertOk()->assertSee(45.65);
      $response->assertOk()->assertSee("2019-06-01 19:23:52");
  }

  /** @test */
  public function delete_a_billing()
  {
      $billing = factory('App\Billing')->create();

      $response = $this->delete('api/billing/' . $billing->id);

      $response->assertStatus(204);
  }
}
