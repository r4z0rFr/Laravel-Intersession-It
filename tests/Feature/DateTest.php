<?php

namespace Tests\Feature;

use App\Date;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

use Carbon\Carbon;

class DateTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        Passport::actingAs(factory(User::class)->create());
    }

    /** @test */
    public function display_all_date()
    {
      $response = $this->get('api/date');
      $response->assertOk()->assertSee(Date::all());
    }

    /** @test */
    public function display_a_date()
    {
        $response = $this->get('api/date/1');
        $response->assertOk()->assertSee(Date::first()->start_date);
    }

    /** @test */
    public function create_a_date()
    {
        $date = factory('App\Date')->make();

        $response = $this->post('api/date', $date->toArray());
        $response->assertOk()->assertSee($date['start_date']);
        $response->assertOk()->assertSee($date['expected_end_date']);
        $response->assertOk()->assertSee($date['end_date']);
    }

    /** @test */
    public function update_a_date()
    {
        $date = factory('App\Date')->create();

        $response = $this->put('api/date/' . $date->id,
                                [ 'start_date' => "2018-01-01 08:15:00",
                                  'expected_end_date' => "2019-01-01 17:00:00",
                                  'end_date' => "2019-06-01 19:23:52"
                                ]
                            );
        $response->assertOk()->assertSee("2018-01-01 08:15:00");
        $response->assertOk()->assertSee("2019-01-01 17:00:00");
        $response->assertOk()->assertSee("2019-06-01 19:23:52");
    }

    /** @test */
    public function delete_a_date()
    {
        $date = factory('App\Date')->create();

        $response = $this->delete('api/date/' . $date->id);

        $response->assertStatus(204);
    }

}
