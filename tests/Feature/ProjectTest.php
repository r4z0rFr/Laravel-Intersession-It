<?php

namespace Tests\Feature;

use App\Project;
use App\Date;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        Passport::actingAs(factory(User::class)->create());
    }
    /** @test */
    public function display_all_projects()
    {
        $response = $this->get('api/project');

        $response->assertOk()
                ->assertSee(Project::all());
    }

    /** @test */
    public function display_a_project()
    {
        $response = $this->get('api/project/1');

        $response->assertOk()
                ->assertSee(Project::first()->name);
    }

    /** @test */
    public function create_a_project()
    {

        $project = factory('App\Project')->make()->toArray();

        $date = factory('App\Date')->make()->toArray();

        $collection = collect();

        $request = $collection->merge([$project, $date])->flattenKeepKeys();

        $response = $this->post('api/project', $request->toArray());

        $response->assertOk()
                ->assertSee($project['name']);
    }

    /** @test */
    public function update_a_project()
    {
        $project = factory('App\Project')->create();

        $response = $this->put('api/project/' . $project->id, ['technical_level' => 2]);

        $response->assertOk()
                ->assertSee(2);
    }

    /** @test */
    public function delete_a_project()
    {
        $risk = factory('App\Project')->create();

        $response = $this->delete('api/project/' . $risk->id);

        $response->assertStatus(204);
    }

    /** @test */
    public function create_task_from_project()
    {
        $project = factory(Project::class)->create();
        $date = factory(Date::class)->create();

        $project->tasks()->create(['date_id' => 1, 'name' => 'Create Gantt', 'technical_level' => 1]);

        $this->assertContains('Create Gantt', $project->tasks()->first()->name);
    }

    /** @test */
    public function add_date_to_project()
    {
        $project = factory(Project::class)->create();

        $date = factory(Date::class)->make();

        $project->date()->create($date->toArray());

        $this->assertEquals($project->date()->first()->starting_date, $date->starting_date);
    }

    /** @test */
    public function assign_user_to_a_project()
    {
        $user = factory(User::class)->create();

        $project = factory(Project::class)->create();

        $project->assign($user);

        $this->assertTrue($project->isAssign($user));
    }
}
