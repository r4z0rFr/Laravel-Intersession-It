<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;
use App\Company;
use App\Customer;

use App\Task;
use App\Tariff;
use App\Date;

class Billing extends Model
{
    protected $fillable =
    [
        // project datas
        'project_id',
        'project_name',

        // company datas
        'company_id',
        'company_name',
        'company_address',
        'company_phone',
        'company_email',
        'company_siret',

        // customer datas
        'customer_id',
        'customer_name',
        'customer_email',
        'customer_phone',
        'customer_siret',

        // price datas
        'total_price',
        'total_budget',
        'billing_date'
    ];


    private function project()
    {
        return Project::find($this->project_id);
    }

    private function company()
    {
        return Company::find($this->company_id);
    }

    private function customer()
    {
        return Customer::find($this->customer_id);
    }


    /*
      To compute the total price of the project,
      we first check out if the project has a company
      associated to it, since it's company associated to
      the project which gives the tarriff.

      The total price of the project is computed by
      adding all the prices of the tasks associated to the
      project.
    */
    public function computePriceOfProject()
    {
        $price = 0;

      /*
        Checking out if the project/tarriff has a companny associated to them.
        If no, then the total_price will be equal to zero.
      */
      if($this->company_id != null)
      {
        /*
          For each task associated to the project,
          we compute its price and add to the total price,
          of the project.
          The computation of the price's task is done by
          computePriceOfATask.
        */
        foreach($this->project()->tasks() as $task)
        {
          $price += computePriceOfATask($task);
        }
      }

      $this->total_price = $price;

    }

    /*
      Compute the price of a task associated to the project
      of the billing.
    */
    private function computePriceOfATask(Task $task)
    {
      /*
        Since we will need to retrieve
        both the tarriff and the daily hours worked of the company.

        We store the company associated to the project/billing
        in a variable called $company.

      */
      $company = $this->company();


      /*
        First we need to get the tarriff of the company which has the
        same technical_level of the task.

        Then once we've retrieved the corresponding tarriff associated to the task,
        we retrieve the price per hour of the tarriff which is store in $price_per_hour.
      */
      $tariff = $company->tariffs()->where('technical_level', $task->technical_level)->first();
      $price_per_hour = $tariff->price;

      /*
        We will also need to retrieve the dates of the task, and the daily hours worked,
        so that we could compute the total numbers of hours worked on the task,
        and multiply it with the price per hour.
      */
      $taskDate = $task->date()->first();
      $daily_hours_worked = $company->daily_hours_worked;

      /*
        Since the start_date, expected_end_date and end_date of the task is stored in the form of a string,
        we convert the string into a DateTime and store them respectively in $start_date and $end_date.
      */
      $start_date = new DateTime($taskDate->start_date);
      /*
        We need to check date is specified or not, if it isn't then it's most likely that the task is still on going.
        If the task is still ongoing, then we compute the total number of hours worked by considering the expected end date
        of the task.
      */
      $end_date = ($taskDate->end_date == null) ? new DateTime($taskDate->expected_end_date) : new DateTime($taskDate->end_date);


      /*
        Once we have the start date and end date of the task, we differentiate both dates
        and store this diff in $interval.
      */
      $interval = $end_date->diff($start_date);
      /*
        The we get the total number of days between both dates.
      */
      $nbOfDays = $interval->days;

      /*
        Also we need to remove all sundays and saturdays, since they're not worked days,
        from the total number of days of the task.
      */
      $period = new DatePeriod($start_date, new DateInterval('P1D'), $end_date);

      foreach($period as $date)
      {
          $currDay = $date->format('D');

          if ($currDay == 'Sat' || $currDay == 'Sun') {
            $nbOfDays--;
          }
      }


      /*
        The total price is the result of the multiplication between
          - the number of days of the task
          - the daily hours worked of the company
          - the price per hour of the task.
      */
      return ($nbOfDays * $daily_hours_worked * $price_per_hour);

    }
}
