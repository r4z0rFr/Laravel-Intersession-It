<?php

namespace App\Traits;

use App\User;

trait Assignable
{

    public function users()
    {
        return $this->morphToMany('App\User', 'assignable');
    }

    public function assign($user)
    {
        $this->users()->attach($user);
    }

    public function unassign(User $user)
    {
        if ($this->isAssign($user)) {
            $this->user($user)->delete();
        }

        return false;
    }

    public function isAssign(User $user)
    {
        return $this->users()->where('user_id', $user->id)->exists();
    }
}
