<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $fillable = [
    'datable_id',
    'datable_type',

    'start_date',
    'expected_end_date',
    'end_date'
    ];

    public $timestamps = false;

    protected $dates = [
    'start_date',
    'expected_end_date',
    'end_date'
    ];

    public function datable()
    {
        return $this->morphTo();
    }
}
