<?php

namespace App\Listeners;

use App\Project;
use App\Billing;

use App\Events\ProjectBillingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProjectBillingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProjectBillingEvent  $event
     * @return void
     */
    public function handle(ProjectBillingEvent $event)
    {

      /*
        The controller specify which action is done, and according to that
        we call out the corresponding method to update the bill associated
        to the project.
            -> 'create'         : creation of a project, we create then the billing
                                  associated to it with createBill.
            -> 'updateProject'  : project informations updated, so that we update it
                                  in the billing.
            -> 'updateCustomer' : customer of the project is modified so we update
                                  the information in the billing accordingly.
            -> 'updateCompany'  : company of the project is modified so we update
                                  the information in the billing accordingly.
      */
      switch($event->action)
      {
        case("create") :
          $this->createBill($event->project);
          break;
        case("updateProject") :
          $event->project->bill()->project_name = $event->project->name;
          $event->project->bill()->total_budget = $event->project->budget;
          break;
        case("updateCustomer") :
          $this->UpdateBillingCustomer($project);
          break;
        case("updateCompany") :
          $this->UpdateBillingCompany($project);
          break;
      }
    }




    /*
      When we create a new project, we create as well a Bill entry associated
      to the project in the database.
    */
    public function createBill(Project $project)
    {

      $billing = new Billing();


      $billing->project_id        = $project->id;
      $billing->project_name      = $project->name;
      $billing->total_budget      = $project->budget;


      /*
        We check out, if the project has a customer associated to it.
        If the project doesn't have any, we set all the datas related
        to the customer to null.
      */

      $isCustomerNull             = ($project->customer == null);

      $billing->customer_id       = $isCustomerNull ? null : $project->customer->id;

      $billing->customer_name     = $isCustomerNull ? null : $project->customer->name;
      $billing->customer_email    = $isCustomerNull ? null : $project->customer->email;
      $billing->customer_phone    = $isCustomerNull ? null : $project->customer->phone;
      $billing->customer_siret    = $isCustomerNull ? null : $project->customer->siret;

      $billing->total_price       = $isCompanyNull ? 0    : $this->computeBillingTotalPrice($project);


      /*
        We check out, if the project has a company associated to it.
        If the project doesn't have any, we set all the datas related
        to the company to null.
      */

      $isCompanyNull              = ($project->company == null);

      $billing->company_id        = $isCompanyNull ? null : $project->company->id;

      $billing->company_name      = $isCompanyNull ? null : $project->company->name;
      $billing->company_address   = $isCompanyNull ? null : $project->company->address;
      $billing->company_phone     = $isCompanyNull ? null : $project->company->phone;
      $billing->company_email     = $isCompanyNull ? null : $project->company->email;
      $billing->company_siret     = $isCompanyNull ? null : $project->company->siret;


    /*
        The total price is computed automatically by the events, as well as
        the billing date, which is why at the billing's creation associated
        to the project, we set those values to null.
        The events will automically compute them, once the billing is stored
        in the database
    */
    $billing->total_price = 0;
    $billing->billing_date = null;

    $billing->save();
  }


  /*
    Update the customer of the billing associated to the project,
    when the customer is changed.
    This event doesn't take care the case in which the project has the same customer
    but informations of the customer are updated.
    This case is delegated to the CustomerBillingListener.
  */
  public function UpdateBillingCustomer(Project $project)
  {
    $isCustomerNull                   = ($project->customer == null);

    $project->bill()->customer_id     = $isCustomerNull ? null : $project->customer->id;

    $project->bill()->customer_name   = $isCustomerNull ? null : $project->customer->name;
    $project->bill()->customer_email  = $isCustomerNull ? null : $project->customer->email;
    $project->bill()->customer_phone  = $isCustomerNull ? null : $project->customer->phone;
    $project->bill()->customer_siret  = $isCustomerNull ? null : $project->customer->siret;
  }

  /*
    Update the company of the billing associated to the project,
    when the company is changed.
    This event doesn't take care the case in which the project has the same customer
    but informations of the customer are updated.
    This case is delegated to the CustomerBillingListener.
  */
  public function UpdateBillingCompany(Project $project)
  {
    $isCompanyNull              = ($project->company == null);

    $project->bill()->company_id        = $isCompanyNull ? null : $project->company->id;

    $project->bill()->company_name      = $isCompanyNull ? null : $project->company->name;
    $project->bill()->company_address   = $isCompanyNull ? null : $project->company->address;
    $project->bill()->company_phone     = $isCompanyNull ? null : $project->company->phone;
    $project->bill()->company_email     = $isCompanyNull ? null : $project->company->email;
    $project->bill()->company_siret     = $isCompanyNull ? null : $project->company->siret;

    $project->bill()->total_price       = $isCompanyNull ? 0    : $this->computeBillingTotalPrice($project);

  }

  /*
    Utility function which computes the total_price of the billing.
    Set the total_price to 0 if the project doesn't have a company associated to it.
  */

  public function computeBillingTotalPrice(Project $project)
  {
    $project->bill()->total_price = 0;

    foreach($project->tasks as $task)
    {
      $project->bill()->total_price += $task->getPrice();
    }
  }
}
