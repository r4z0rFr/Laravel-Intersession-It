<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TasksResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->resource;

        $formatedResource = collect();

        $collection->each(function ($task) use ($formatedResource) {

            $duration = is_null($task->date) ? null : $task->date->start_date->diffInDays($task->date->end_date);

            $formatedResource->push([
            'id'                => $task->id,
            'text'              => $task->name,
            'technical_level'   => $task->technical_level,
            'project_id'        => $task->project_id,
            'parent'            => 0, //temporary data
            'start_date'        => is_null($task->date) ? null : $task->date->start_date->format('d-m-Y'),
            'duration'          => $duration,
            'owner'             => $task->owner->name,
            'priority'          => 'Faible'
            ]);
        });

        return $formatedResource;
    }
}
