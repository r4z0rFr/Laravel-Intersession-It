<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::with('undertasks')->get()->toJson();

        return response()->json($tasks, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = Project::find($request->project_id);

        $task = $project->tasks()->create($request->only('name', 'priority', 'technical_level', 'user_id'));

        $date = $task->date()->create($request->only('start_date', 'end_date', 'expected_end_date'));

        return response()->json($task, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        /*
          Add :
          - calendar
          - cost
        */
        return response()->json($task->load('undertasks'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        /*
          Add :
          - calendar
          - cost
        */
        return response()->json($task::with('undertasks'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Task                $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $task->update($request->all());

        return response()->json($task->load('undertasks'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        /*
          Task can have subtask and has it is we want to destroy all the task's
          children, which is why we don't use the detach function before deleting it.
        */
        $task->delete();

        return response()->json(null, 204);
    }
}
