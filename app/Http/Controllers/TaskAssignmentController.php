<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TaskAssignmentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user, Task $task)
    {
        $task->assign($user);

        return response()->json($task, 200);
    }

    public function delete(User $user, Task $task)
    {
        if ($task->unassign($user) == false) {
            return response()->json($task->load('user'), 414);
        }

        return response()->json($task->load('user'), 200);
    }
}
