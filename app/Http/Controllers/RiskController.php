<?php

namespace App\Http\Controllers;

use App\Risk;
use App\Task;
use Illuminate\Http\Request;

class RiskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $risks = Risk::all();

        return response()->json($risks, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $risk = Risk::create($request->all());

        return response()->json($risk, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Risk $risk
     * @return \Illuminate\Http\Response
     */
    public function show(Risk $risk)
    {
        return response()->json($risk, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Risk $risk
     * @return \Illuminate\Http\Response
     */
    public function edit(Risk $risk)
    {
        return response()->json($risk, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Risk                $risk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Risk $risk)
    {
        $risk->update($request->all());

        return response()->json($risk, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Risk $risk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Risk $risk)
    {
        $risk->delete();

        return response()->json(null, 204);
    }
}
