<?php

namespace App\Http\Controllers;

use App\Billing;
use Illuminate\Http\Request;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $billings = Billing::all();

        return response()->json($billings, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $billing = Billing::create($request->all());

        return response()->json($billing, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Billing $billing
     * @return \Illuminate\Http\Response
     */
    public function show(Billing $billing)
    {
        return response()->json($billing, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Billing $billing
     * @return \Illuminate\Http\Response
     */
    public function edit(Billing $billing)
    {
        return response()->json($billing, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Billing             $billing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Billing $billing)
    {
        $billing->update($request->all());

        return response()->json($billing, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Billing $billing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Billing $billing)
    {
        $billing->delete();

        return response()->json(null, 204);
    }
}
