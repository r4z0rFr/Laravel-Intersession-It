<?php

namespace App;

use App\Traits\Domainable;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

  use Domainable;

    protected $fillable = [
      'name',
      'address',
      'phone',
      'email',
      'daily_hours_worked',
      'siret',

      'user_id'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function tariffs()
    {
        return $this->hasMany('App\Tariff');
    }

    public function bills()
    {
      return Billing::where('company_id', $this->id);
    }
}
