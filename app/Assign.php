<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assign extends Model
{
    protected $guarded = [];


    public function tasks()
    {
        return $this->morphedByMany('App\Task', 'assignable');
    }

    public function projects()
    {
        return $this->morphedByMany('App\Project', 'assignable');
    }
}
