<?php

namespace App\Events;

use App\Project;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProjectBillingEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $project;
    public $action;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($action, Project $project)
    {
        $this->project = $project;
        $this->action = $action;
    }

}
