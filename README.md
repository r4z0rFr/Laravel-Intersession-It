# Laravel-Intersession-It

This is a school project , it will not be used has commercial purposes.

This is the Backend side of the project used for the Api

## Installation

### Clone and install

    $ git clone git@gitlab.com:r4z0rFr/Laravel-Intersession-It.git
    $ cd aravel-Intersession-It
    $ composer install

### Setup Environement

Our application need a database to work

you have a bunch of choices : Mysql , PostgreSql , Sqlite

refer to laravel documentation for the Database Configuration : [Laravel Database Configuration](https://laravel.com/docs/5.6/database)

    $ cp .env.example .env
    $ php artisan migrate:fresh --seed
    $ vendor/bin/phpunit

A default user will be created with the given logs :

Username: admin
Password: admin


### Database model

<img src="https://files.slack.com/files-pri/TBQQ9J13N-FC1V7HLTW/download/dbb.svg">

