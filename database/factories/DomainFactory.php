<?php

use Faker\Generator as Faker;

$factory->define(App\Domain::class, function (Faker $faker) {
    return [
        'name'              => $faker->word,
        'domainable_id'     =>  1,
        'domainable_type'   => 'App\Project'
    ];
});
