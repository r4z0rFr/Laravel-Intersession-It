<?php

use Faker\Generator as Faker;

$factory->define(App\Billing::class, function (Faker $faker) {
    return [
        'project_name'    => $faker->name,

        'company_name'    => $faker->name,
        'company_address' => $faker->address,
        'company_phone'   => $faker->e164PhoneNumber,
        'company_email'   => $faker->unique()->safeEmail,
        'company_siret'   => "732 829 320 00074",

        'customer_name'   => $faker->name,
        'customer_email'  => $faker->unique()->safeEmail,
        'customer_phone'  => $faker->e164PhoneNumber,
        'customer_siret'  => "732 829 320 00074",

        'total_price'     => 2150.12,
        'total_budget'    => 532.52,
        'billing_date'    => $faker->dateTimeBetween('+1 years', '+2years')
    ];
});
