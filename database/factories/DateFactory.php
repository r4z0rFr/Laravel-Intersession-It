<?php

use App\Date;
use Faker\Generator as Faker;

$factory->define(Date::class, function (Faker $faker) {
    return [
      'datable_id'          => 1,
      'datable_type'        => 'App\Task',

      'start_date'          => $faker->dateTimeBetween('+0 days', '+10 days'),
      'expected_end_date'   => $faker->dateTimeBetween('+10 days', '+20 days'),
      'end_date'            => $faker->dateTimeBetween('+20 days', '+30 days')
    ];
});
