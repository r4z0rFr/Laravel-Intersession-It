<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'name'                => $faker->title,
        'email'               => $faker->email,
        'phone'               => $faker->e164PhoneNumber,
        'address'             => $faker->address,
        'description'         => $faker->sentence,
        'siret'               => $faker->sentence,

        'daily_hours_worked'  => 8,

        'user_id'       => 1
    ];
});
